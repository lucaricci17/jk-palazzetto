---
layout: home
gallery:
- "/uploads/pexels-photo-219943.jpg"
- "https://placehold.it/1920x800"
- "https://placehold.it/1920x800"
---
### «a juorney along the brigand’s path»


«the brigand paths» project has been realized for those people who love nature, outdoor activities, relax and good food. Our specialized guides let you discover magnificent areas follow brigand’s tracks. A lot of itineraries, suitable for several needs, are available for walking, bicycle or horse-riding tours and its let you go through the beautiful territory rich of forests, lakes, hills and colorful fields.

the houses tours are daily organized to reach all the archaeological sites of the area, the etruscan tombs of sovana and the rocky settlement of vitozza and san rocco. Moreover you can go along the etruscan roads of pitigliano and sorano.
Activities are also open to people who don’t have equestrian experience. We can organize indivudual or collective lessons to teach the basic principles of horse riding.

### Coming soon: nordic walking!!


Our guides organize walking tours to know the lives of brigands and to get in touch with it.  
You can choose easy walking to the most demanding routes of the selva del lamone. The recommended trails are distinguished by typology and degree of difficulty, accommodating both families and children and the most enterprising hikers!

Our guides have studied and prepared the best mtb trails in maremma laziale for those who love the off-road trails and we offer the possibility of booking mountain bike guides, whatever your level of experience in this sport! Our guide, knowing the area perfectly, is able to organize the routes according to the needs of our customers, by guiding you on paths or white roads that are appropriate to your driving level, through the best corners of the maremma laziale.
Come and discover the our mtb gps trails, and let yourself be traversed through our forests and hills, along paths that can reach the sea. We’ve put your fun first and thanks to our knowledge of the best bike routes in our area, we’ll let you pass the best mtb day ever.
Take your bike to us, or take advantage of our rental service, always active. You will find the bike for you, always ready to wait for you.