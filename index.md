---
layout: home
gallery:
  - "/assets/img/palas-7.jpg"
  - "/assets/img/palas-11.jpg"
  - "/assets/img/palas-1.jpg"
---

Nel centro dello splendido borgo di Bagnaia (Viterbo), si trova il B&amp;B Il Palazzetto.
A due passi da Villa Lante, uno degli esempi più belli dell’architettura rinascimentale e di giardino all’italiana.

Soggiornando nella nostra struttura l’ospite avrà inoltre la possibilità di visitare, Viterbo con il suo centro storico medioevale più grande d’Italia, città d’arte situate a pochi chilometri, borghi caratteristici e unici come “Civita di Bagnoregio” e noti siti archeologici ed ammirare la natura incontaminata dei laghi di Vico e Bolsena, oppure immergersi nelle acque delle Terme dei Papi raggiungibili in 5 minuti. Nelle vicinanze della struttura potrete trovare parcheggio gratuito, ristoranti-pizzerie.
