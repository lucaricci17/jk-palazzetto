---
title: Green Room
category: Camere
layout: content
gallery:
  - "/assets/img/palas-87.jpg"
  - "/assets/img/palas-92.jpg"
gallery-light:
  - "/assets/img/palas-87.jpg"
  - "/assets/img/palas-88.jpg"
  - "/assets/img/palas-89.jpg"
  - "/assets/img/palas-90.jpg"
  - "/assets/img/palas-93.jpg"
  - "/assets/img/palas-94.jpg"
  - "/assets/img/palas-96.jpg"
  - "/assets/img/palas-98.jpg"
---

La spaziosa camera è composta da soggiorno con tavolo, poltrona, questa camera matrimoniale, TV e riscaldamento autonomo. Il comodo bagno, è arricchito da un lavandino in peperino realizzato dalle sapienti mani di uno scalpellino locale e completato dalla doccia con piatto sempre in peperino.
