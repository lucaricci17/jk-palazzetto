---
category: Camere
layout: content
gallery:
  - "/assets/img/palas-63.jpg"
  - "/assets/img/palas-73.jpg"

date: 2019-02-02 12:32:06 +0000
gallery-light:
  - "/assets/img/palas-67.jpg"
  - "/assets/img/palas-68.jpg"
  - "/assets/img/palas-69.jpg"
  - "/assets/img/palas-70.jpg"
  - "/assets/img/palas-71.jpg"
  - "/assets/img/palas-72.jpg"
  - "/assets/img/palas-73.jpg"
  - "/assets/img/palas-74.jpg"
  - "/assets/img/palas-75.jpg"
  - "/assets/img/palas-76.jpg"
  - "/assets/img/palas-77.jpg"
  - "/assets/img/palas-63.jpg"
  - "/assets/img/palas-64.jpg"
---

E’ una camera matrimoniale, meno ampia rispetto alle altre, ma gode di tutti i confort quali, aria ondizionata, riscaldamento autonomo e TV. Il bagno privato è impreziosito da lavandino in peperino, ampia doccia con piatto in peperino.
