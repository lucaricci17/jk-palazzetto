---
category: Camere
layout: content

gallery:
  - "/assets/img/palas-20.jpg"
  - "/assets/img/palas-23.jpg"

date: 2019-02-02 12:32:59 +0000
gallery-light:
  - "/assets/img/palas-24.jpg"
  - "/assets/img/palas-28.jpg"
  - "/assets/img/palas-30.jpg"
  - "/assets/img/palas-31.jpg"
  - "/assets/img/palas-32.jpg"
  - "/assets/img/palas-34.jpg"
  - "/assets/img/palas-36.jpg"
  - "/assets/img/palas-37.jpg"
  - "/assets/img/palas-25.jpg"
  - "/assets/img/palas-26.jpg"
---

E’ la camera più suggestiva e panoramica, si affaccia con il suo terrazzo sulla piazza principale del Borgo di Bagnaia, dove gli ospiti possono godersi su richiesta e nei periodi da maggio a settembre, la colazione all’aperto. La camera è arredata con mobili degli anni ’30, inclusi i comodini e il cassettone. Il bagno, come per gli altri, è sempre caratterizzato da lavandino in peperino sorretto da staffe in ferro battuto e doccia con piatto in peperino.
